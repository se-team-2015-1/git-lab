program Lab02;

const
    MatrixDimension = 6;
    HighBound = 20;
    LowBound = -17;
    NumbersQuantity = HighBound - LowBound + 1;

type
    SquareMatrix = array [1..MatrixDimension, 1..MatrixDimension] of Integer;
    Index = record
        Row: 1..MatrixDimension;
        Column: 1..MatrixDimension;
    end;

function GetNumber: Integer;
begin
    GetNumber := Random(NumbersQuantity) + LowBound;
end;

procedure FillMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to matrixDimension do
    begin
        for j := 1 to matrixDimension do
        begin
            matrix[i, j] := GetNumber;
        end;
    end;
end;

procedure PrintMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixDimension do
    begin
        for j := 1 to MatrixDimension do
        begin
            Write (matrix[i, j]:4);
        end;
        WriteLn;
    end;
end;

function GetMaxBelowMainDiagonal(matrix: SquareMatrix): Index;
var
    i, j, max: Integer;
    maxIndex: Index;
begin
    max := matrix[2, 1];
    with maxIndex do
    begin
        Row := 2;
        Column := 1;
        for i := Row+1 to MatrixDimension do
        begin
            for j := 1 to i-1 do
            begin
                if matrix[i, j] > max then
                begin
                    max := matrix[i, j];
                    Row := i;
                    Column := j;
                end;
            end;
        end;
    end;
    GetMaxBelowMainDiagonal := maxIndex;
end;

function GetMaxOnMainDiagonal(var matrix: SquareMatrix): Index;
var
    i, max: Integer;
    maxIndex: Index;
begin
    with maxIndex do
    begin
        Row := 1;
        Column := 1;
        max := matrix [Row, Column];
        for i := 2 to MatrixDimension do
        begin
            if matrix[i, i] > max then
            begin
                max := matrix[i, i];
                Row := i;
                Column := i;
            end;
        end;
    end;
    with GetMaxOnMainDiagonal do
    begin
        Row := maxIndex.Row;
        Column := maxIndex.Column;
    end;
end;

procedure PrintElement(elementIndex: Index; matrix: SquareMatrix);
begin
    with elementIndex do
    begin
        WriteLn(matrix[Row, Column], ' [', Row, ', ', Column, ']');
    end;
end;

procedure ExchangeElements(var matrix: SquareMatrix);
var
    first, second: Index;
    bufer: Integer;
begin
     first := GetMaxBelowMainDiagonal(matrix);
     second := GetMaxOnMainDiagonal(matrix);
     bufer := matrix[first.Row, first.Column];
     matrix[first.Row, first.Column] := matrix[second.Row, second.Column];
     matrix[second.Row, second.Column] := bufer;
end;

var
    matrix: SquareMatrix;

begin
    Randomize;
    WriteLn('Square matrix');
    WriteLn;
    FillMatrix(matrix);
    PrintMatrix(matrix);
    WriteLn;
    Write('Max of elements below main diagonal: ');
    PrintElement(GetMaxBelowMainDiagonal(matrix),matrix);
    WriteLn;
    Write('Max of main diagonal: ');
    PrintElement(GetMaxOnMainDiagonal(matrix),matrix);
    WriteLn;
    ExchangeElements(matrix);
    WriteLn('Matrix after exchange of these elements');
    WriteLn;
    PrintMatrix(matrix);
    ReadLn;
end.
